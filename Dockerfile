# syntax=docker/dockerfile:1

# Comments are provided throughout this file to help you get started.
# If you need more help, visit the Dockerfile reference guide at
# https://docs.docker.com/go/dockerfile-reference/

# Want to help us make this template better? Share your feedback here: https://forms.gle/ybq9Krt8jtBL3iCk7

ARG NODE_VERSION=18.0.0

FROM node:${NODE_VERSION}-alpine

USER root 

RUN apk update

RUN apk add bash

RUN apk add git

RUN apk add --no-cache nodejs npm

RUN apk add py3-pip

RUN python3 -m pip install --no-deps -U yt-dlp

RUN apk add ffmpeg

# Use production node environment by default.
ENV NODE_ENV production

# Download dependencies as a separate step to take advantage of Docker's caching.
# Leverage a cache mount to /root/.npm to speed up subsequent builds.
# Leverage a bind mounts to package.json and package-lock.json to avoid having to copy them into
# into this layer.
RUN --mount=type=bind,source=package.json,target=package.json \
    --mount=type=bind,source=package-lock.json,target=package-lock.json \
    --mount=type=cache,target=/root/.npm \
    npm ci --omit=dev

# Copy the rest of the source files into the image.
WORKDIR /home/node/app

RUN git clone https://gitlab.com/script_dump/youtube_to_mp3.git /home/node/app/

RUN npm install

RUN chmod 777 to_mp3.sh 

# Expose the port that the application listens on.
EXPOSE 3000

# Run the application.
CMD npm run start