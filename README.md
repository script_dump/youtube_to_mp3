# youtube_to_mp3

## What is this ?
Small Node.js server utilizing yt-dlp and ffmpeg to convert youtube videos to mp3 files.

## How to use ?

- [Go to the latest release](https://gitlab.com/script_dump/youtube_to_mp3/-/releases/permalink/latest)
- Download the Dockefile image
- Run the Dockerfile or the docker-compose.yml

The server run on port 3000 by default, your HTTP rquest must follow this syntax : 
```JSON
{
    "vName" : "video_name",
    "vUrl" : "youtube_url"
}
```