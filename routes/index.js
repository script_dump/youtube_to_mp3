const express = require('express');
const path = require('path');
const router = express.Router();
const execSync = require('child_process').execSync;

router.post('/',async (req, res) => {
  execSync(('./to_mp3.sh '+req.body["vUrl"]+' tmp/'+req.body["vName"]),{ encoding: 'utf-8' })
  const videoPath = 'tmp/'+req.body["vName"]+'.mp3';
  res.sendFile(path.resolve(videoPath), {root: ''});
})

router.delete('/', async (req, res) => {
  execSync('rm tmp/*',{ encoding: 'utf-8' })
  res.send('Videos deleted');
} )

module.exports = router;
